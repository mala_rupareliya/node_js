const pg = require("pg")
const secretManager = require("../aws/awsSecretService.js")

class Database {



  constructor() {

    this.DB_NAME = {
      USER_POOL: 'user_pool',
      CONTENT_POOL: 'content_pool'
    }

    this._dbMap = {}
    if (process.env.DF_ENV == "dev") {
      console.log('in dev');

      this._userPool = new pg.Pool({
        database: process.env.DB_USER_DATABASE,
        user: process.env.DB_USER_NAME,
        password: process.env.DB_USER_PASSWORD,
        host: process.env.DB_USER_HOST,
        port: process.env.DB_USER_PORT,
        max: 1,
        application_name: "df_user",
        connectionTimeoutMillis: 10000,
        idleTimeoutMillis: 500
      });


      this._conetnePool = new pg.Pool({
        database: process.env.DB_CONTENT_DATABASE,
        user: process.env.DB_CONTENT_NAME,
        password: process.env.DB_CONTENT_PASSWORD,
        host: process.env.DB_CONTENT_HOST,
        port: process.env.DB_CONTENT_PORT,
        max: 1,
        application_name: "df_content",
        connectionTimeoutMillis: 10000,
        idleTimeoutMillis: 500
      });
    } else {

      console.log('in prod');

      this._userPool = new pg.Pool({
        database: "df_users",
        max: 1,
        application_name: "df_user",
        connectionTimeoutMillis: 10000,
        idleTimeoutMillis: 500
      });


      this._conetnePool = new pg.Pool({
        database: "df_content",
        max: 1,
        application_name: "df_content",
        connectionTimeoutMillis: 10000,
        idleTimeoutMillis: 500
      });

    }

    const userPool = this._userPool
    const contentPool = this._conetnePool
    const runQueryFunction = this.runQuery

    this._dbMap[this.DB_NAME.USER_POOL] = {
      query: async function (databaseQuery) {
        return runQueryFunction(userPool, process.env.SECRET_RDS_USERS, databaseQuery)
      }
    }
    this._dbMap[this.DB_NAME.CONTENT_POOL] = {
      query: async function (databaseQuery) {
        return runQueryFunction(contentPool, process.env.SECRET_RDS_CONTENT, databaseQuery)
      }
    }

    console.log("Database constructor")
  }

  async runQuery(pool, secretKeyConst, databaseQuery) {
    // console.log("runQuery")
    const options = pool.options

    if (!options.user) {
      // console.log("runQuery before secret manager")
      const secret = await secretManager.getSecretValue(secretKeyConst)
      // console.log("runQuery after secret manager")
      console.log(secret)
      pool.options.user = secret.username
      pool.options.password = secret.password
      pool.options.host = secret.host
      pool.options.port = secret.port

    }

    return pool.query(databaseQuery)
  }

  getDBConn(name) {
    return this._dbMap[name]
  }

}
module.exports = new Database()