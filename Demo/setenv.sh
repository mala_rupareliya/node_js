
export CORE_PATH="/Users/malaruparel/DemoProject/core/"

export DF_ENV="dev"
export AWS_REGION="eu-west-1"
export SECRET_RDS_USERS="secret-rds-users-dev"
export SECRET_RDS_CONTENT="secret-rds-content-dev"

export REDIS_HOST="localhost"
export REDIS_PORT=6379

export DB_USER_HOST="poc-database-1.cy0b4mwsn310.eu-west-1.rds.amazonaws.com"
export DB_USER_NAME="postgres"
export DB_USER_PASSWORD="DET.POC.123"
export DB_USER_DATABASE="df_users"
export DB_USER_PORT=5432

export DB_CONTENT_HOST="poc-database-1.cy0b4mwsn310.eu-west-1.rds.amazonaws.com"
export DB_CONTENT_NAME="postgres"
export DB_CONTENT_PASSWORD="DET.POC.123"
export DB_CONTENT_DATABASE="df_content"
export DB_CONTENT_PORT=5432